---
layout: article
title: 在 Windows 下安装 Jekyll
categories:
  - Ruby
tags:
  - Jekyll
published: false
comments: true
---

1.  安装 Ruby
2.  安装 DevKit
3.  安装 Jekyll

在安装 Ruby 和 DevKit 时，**不要安装在包含空格的文件夹中**，否则在安装 Jekyll 时会报错！

{% highlight sh %}
gem install jekyll
{% endhighlight %}
