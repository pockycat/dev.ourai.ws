---
title: 层叠样式表
description: 使你了解层叠样式表是如何让网页千变万化的
permalink: /tutorials/css/
---

层叠样式表，即 Cascading Style Sheets（下文用「CSS」来指代），是一种由哈坤·利（Håkon Wium Lie）与伯特·波斯（Bert Bos）合作设计并开发，用于描述结构化文档（如 HTML、XML）表现形式的样式表语言。

设计 CSS 的主要目的是**使文档的内容与表现相分离**，包括布局、颜色和字形等元素。这样一来就能够提高内容可访问性，提供更大的灵活性，使多个页面共享格式，减少内容结构的复杂度和重复。

## 笔记
{:.heading}

- [层叠样式表基础](basics/)
