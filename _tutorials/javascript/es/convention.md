---
title: ECMAScript 约定
description: 关于 ECMAScript 教程的一些约定
css:
  - highlight
---

* 所述内容以 [ECMAScript 5](http://es5.github.io/){:target="_blank"}{:rel="external nofollow"} 为基准；
* 「literal」既叫「直接量」又叫「字面量」，文中统一为「直接量」。
