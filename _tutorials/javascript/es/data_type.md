---
title: ECMAScript 数据类型
description: ECMAScript 中的数据类型
css:
  - highlight
---

ECMAScript 中定义了 6 种数据类型，其中 5 种是原始类型。

## 原始类型
{:.heading}

原始类型（primitive type）是没有任何属性和方法的非对象的数据类型：

- 字符串（String）
- 数字（Number）
- 布尔值（Boolean）
- 空值（Null）
- 未定义（undefined）

所有的原始类型均为不可变类型。

## 对象类型

名/值对集合
