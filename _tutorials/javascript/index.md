---
title: JavaScript
description: 
permalink: /tutorials/javascript/
---

「JavaScript」这个词在不同语境下可以指代很多东西：

- Sun Microsystem（现 Oracle）的注册商标
- Netscape（现 Mozilla）对 ECMAScript 的实现
- 各厂商对 ECMAScript 在客户端的实现
- 各厂商对 ECMAScript 在客户端的实现及 W3C 制定的 API
- Node.js 等 ECMAScript 在服务端的实现
- 客户端的实现与服务端的实现
- ……

无论「JavaScript」代表什么，然而其所有含义中的共通部分，也就是其核心，是不包括输入输出，只能针对字符串、数组、日期和正则表达式等进行处理的 ECMAScript。它是学会并掌握 JavaScript 的基础和关键！

## 客户端 JavaScript
{:.heading}

- [ECMAScript](es/)
- DOM
